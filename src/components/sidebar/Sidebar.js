import React from 'react';

import { Link } from 'dva/router';
import { Row, Col, Icon } from 'antd';

import classnames from 'classnames';

import styles from './sidebar.css';
const { linkItem, dashboard } = styles;

const Sidebar = ({ location }) => {
  const { pathname } = location;

  return (
    <div className={styles.sidebar}>
      <div className={styles.logo}>内部管理系统</div>
      <div className={styles.sidebarBody}>
        <Row className="marginBottomMd">
          <Col span={24}>
            <Link to={"/dashboard"} className={classnames(linkItem, dashboard, {active: pathname === '/dashboard'})}>
              <Icon type="dashboard" className="marginRightMd" style={{fontSize: "50px"}}/>
              <span>总览</span>
            </Link>
          </Col>
        </Row>
        <Row gutter={10} className="marginBottomMd">
          <Col span={12}>
            <Link to={"/contract"} className={classnames(linkItem, {active: pathname==="/contract"})}>
              <div>
                <Icon type="book" style={{fontSize: "40px", margin: "15px 0"}}/>
              </div>
              <div>合同管理</div>
            </Link>
          </Col>
          <Col span={12}>
            <Link to={"/customer"} className={classnames(linkItem, {active: pathname==="/customer"})}>
              <div>
                <Icon type="contacts" style={{fontSize: "40px", margin: "15px 0"}}/>
              </div>
              <div>客户管理</div>
            </Link>
          </Col>
        </Row>
        <Row gutter={10} className="marginBottomMd">
          <Col span={12}>
            <Link to={"/employment"} className={classnames(linkItem, {active: pathname==="/employment"})}>
              <div>
                <Icon type="team" style={{fontSize: "40px", margin: "15px 0"}}/>
              </div>
              <div>员工管理</div>
            </Link>
          </Col>
          <Col span={12}>
            <Link to={"/file"} className={classnames(linkItem, {active: pathname==="/file"})}>
              <div>
                <Icon type="file-text" style={{fontSize: "40px", margin: "15px 0"}}/>
              </div>
              <div>文件管理</div>
            </Link>
          </Col>
        </Row>
        <Row gutter={10} className="marginBottomMd">
          <Col span={12}>
            <Link to={"/setting"} className={classnames(linkItem, {active: pathname==="/setting"})}>
              <div>
                <Icon type="setting" style={{fontSize: "40px", margin: "15px 0"}}/>
              </div>
              <div>设置</div>
            </Link>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default Sidebar;