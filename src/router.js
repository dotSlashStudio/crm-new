import React from 'react';
import { Route, Switch, Redirect, routerRedux } from 'dva/router';

import App from './routes/App';
import dynamic from 'dva/dynamic';

// import DashboardPage from './routes/dashboard/DashboardPage';
// import ContractPage from './routes/contract/ContractPage';

const { ConnectedRouter } = routerRedux;

const Routers = function({ history, app }) {

  //const error = dynamic
  const routes = [
    {
      path: '/dashboard',
      component: () => import('./routes/dashboard/DashboardPage')
    },
    {
      path: '/contract',
      models: () => [import('./models/contract')],
      component: () => import('./routes/contract')
    },
    {
      path: '/customer',
      component: () => import('./routes/dashboard/DashboardPage')
    },
    {
      path: '/employment',
      component: () => import('./routes/dashboard/DashboardPage')
    },
    {
      path: '/setting',
      component: () => import('./routes/dashboard/DashboardPage')
    }
  ]

  return (
    <ConnectedRouter history={history}>
      <App>
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/dashboard" />)} />
          {
            routes.map(({ path, ...dynamics }, key) => {
              console.log(...dynamics)
              return (
                <Route key={key}
                  exact
                  path={path}
                  component={dynamic({
                    app,
                    ...dynamics
                  })}
                />
              )
            })
          }
        </Switch>
      </App>
    </ConnectedRouter>
  );
}

export default Routers;
