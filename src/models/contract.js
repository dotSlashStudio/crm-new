import _ from 'lodash';
import * as contractService from "../services/contract";
import queryString from 'query-string';
import modelExtend from 'dva-model-extend';
import { pageModel } from './common';
import { message } from 'antd';

export default modelExtend(pageModel, {
  namespace: 'contract',
  state: {
    currentItem: {},
    editModalType: 'create',
    editModalVisible: false,
    detailModalVisible: false,
    conditionModalVisible: false
  },
  reducers: {
    // save(state, {payload: { list, total, page }}) {
    //   return { ...state, list, total, page };
    // },
    showConditionModal(state) {
      return {
        ...state,
        conditionModalVisible: true
      }
    },
    hideConditionModal(state) {
      return {
        ...state,
        conditionModalVisible: false,
      }
    },
    showEditModal(state, { payload }) {
      return {
        ...state,
        ...payload,
        editModalVisible: true,
      }
    },
    hideEditModal(state) {
      return {
        ...state,
        editModalVisible: false,
      }
    },
    showDetailModal(state, { payload }) {
      return {
        ...state,
        ...payload,
        detailModalVisible: true
      }
    },
    hideDetailModal(state) {
      return {
        ...state,
        detailModalVisible: false
      }
    }
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const remote = yield call(contractService.fetch, payload);
      yield put({
        type: 'querySuccess',
        payload: {
          list: _.get(remote, 'data'),
          pagination: {
            current: Number(payload.page) || 1,
            total: _.get(remote, 'count'),
          },
        }
      })
    },
    *create({ payload: values}, { call, put }) {
      const remote = yield call(contractService.create, values);
      if (remote) {
        yield put({ type: 'hideEditModal' });
        yield put({ type: 'reload'});
      } else {
        throw remote
      }
    },
    *remove({ payload }, { call, put}) {
      yield call(contractService.remove, payload);
      yield put({ type: 'reload'});
    },
    *update({ payload }, { call, put, select }) {
      const contractId = yield select(state => _.get(state, 'contract.currentItem.id'));

      payload = {
        ...payload,
        contractId
      };

      const remote = yield call(contractService.update, payload);
      if (remote) {
        yield put({ type: 'hideEditModal' });
        yield put({ type: 'reload'});
      } else {
        throw remote
      }
    },
    *updateInvoice({ payload }, { call, put }) {
      const remote = yield call(contractService.updateInvoice, payload);
      if (remote) {
        yield put({ type: 'hideDetailModal' });
        yield put({ type: 'reload'});
        message.success("数据同步成功！");
      } else {
        throw remote
      }
    },
    *reload(action, { put, select}) {
      // const query = yield select(state => _.get(state, 'routing.location.query'));
      const query = yield select(state => {
        console.log(state);
        return {
          query: _.get(state, 'routing.location.query'),
          ...queryString.parse(_.get(state, 'routing.location.search')),
        }
      })

      yield put({ type: 'fetch', payload: { ...query }});
    },
    *exportFile(action, { call, select }) {
      const query = yield select(state => {
        return _.get(state, 'routing.location.query')
      });
      yield call(contractService.exportFile, query);
    },
    *importFile(action, { call, select }) {
      const query = yield select(state => state.location.query);
      yield call(contractService.importFile, query);
    }
  },
  subscriptions: {
    setup({ dispatch, history}) {

      return history.listen(({ pathname, query, search }) => {
        if(pathname === '/contract') {
          dispatch({
            type: 'fetch',
            payload: {
              query: { ...query },
              ...queryString.parse(search)
            },
          })
        }
      })
    }
  }
})
