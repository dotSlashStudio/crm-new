// import dva from 'dva';
import './index.css';

// console.log("let's getting start!")
// // 1. Initialize
// const app = dva();

// // 2. Plugins
// // app.use({});

// // 3. Model
// // app.model(require('./models/example').default);

// // 4. Router
// app.router(require('./router').default);

// // 5. Start
// app.start('#root');


import { message } from 'antd'
import dva from 'dva'
import createLoading from 'dva-loading'
import createHistory from 'history/createBrowserHistory'

// 1. Initialize
const app = dva({
  ...createLoading({
    effects: true,
  }),
  history: createHistory(),
  onError (error) {
    message.error(error.message)
  },
})

// 2. Model
// app.model(require('./models/app'))

// 3. Router
app.router(require('./router').default)

// 4. Start
app.start('#root')