import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import classnames from 'classnames';

import style from './InvoiceItem.css'

import { Row, Col, Form, Input, DatePicker, Button } from 'antd';
const FormItem = Form.Item;

class InvoiceEdit extends React.Component {

  state = {
    showEdit: _.get(this.props, 'item.id') ? true : false,
    showButton: _.get(this.props, 'item.id') ? false : true,
  }

  handleUpdateClick = () => {
    this.setState({
      showButton: !this.state.showButton,
      showEdit: !this.state.showEdit,
    })
  }

  handleCancelClick = () => {
    const { onAddToggle, onToggleMode, from } = this.props;
    if(onToggleMode) {
      onToggleMode()
    }else {
      onAddToggle();
    }
  }

  handleSubmitClick = () => {
    const { item, index, onInvoiceUpdate, form, onToggleMode } = this.props;
    const { validateFields, getFieldsValue } = form;
    validateFields((errors) => {
      if (errors) {
        return errors
      }
      const data = {
        ...item,
        ...getFieldsValue(),
      }
      console.log("ffrom edit: ", index);
      onInvoiceUpdate(data, index);
      if(onToggleMode) {
        onToggleMode()
      }
    })
  }

  render() {
    const { item, form, onToggleMode } = this.props;
    const { getFieldDecorator } = form;
    return (
      <div className={classnames("marginTopMd", {backLightRed: onToggleMode})}>
        <Form layout="inline" className={style.detail}>
          <Row>
            <Col span={8}>
              <FormItem label="发票编号">
                {getFieldDecorator('id', {
                  initialValue: item.id,
                  rules: [
                    {
                      required: false,
                      message: '必填'
                    },
                  ],
                })(<Input />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="开票金额">
                {getFieldDecorator('amount', {
                  initialValue: item.amount || 0,
                  rules: [
                    {
                      required: false,
                      message: '请输入数值',
                      // type: 'number'
                    },
                  ],
                })(<Input />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="开票日期">
                {getFieldDecorator('createdAt', {
                  initialValue: item.createdAt ? moment(item.createdAt) : null,
                  rules: [
                    {
                      required: false,
                      message: '必填'
                    },
                  ],
                })(<DatePicker />)}
              </FormItem>

            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <FormItem label="实收日期">
                {getFieldDecorator('receivedAt', {
                  initialValue: item.receivedAt ? moment(item.receivedAt) : null,
                  rules: [
                    {
                      required: false,
                      message: '必填'
                    },
                  ],
                })(<DatePicker />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="实收金额">
                {getFieldDecorator('receivedAmount', {
                  initialValue: item.receivedAmount || 0,
                  rules: [
                    {
                      required: false,
                      message: '请输入数值',
                      // type: 'number'
                    },
                  ],
                })(<Input />)}
              </FormItem>
            </Col>
            <Col span={8} className="clearfix" style={{marginTop: '42px'}}>
              <div className="pullLeft marginRightMd">
                <Button type="primary" onClick={this.handleSubmitClick}>确认</Button>
              </div>
              <div className="pullLeft">
                <Button onClick={this.handleCancelClick}>取消</Button>
              </div>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

export default Form.create()(InvoiceEdit)
