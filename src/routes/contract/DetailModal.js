import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import { Modal, Row, Col, Button, message } from 'antd';
import InvoiceItem from './InvoiceItem';
import InvoiceEdit from './InvoiceEdit';

// const DetailModal = ({
//   item,
//   onOk,
//   ...detailModalProps
// }) => {


//   const modalOpts = {
//     ...detailModalProps,
//     okText: '确认',
//     cancelText: '取消',
//   }

//   return (
//     <Modal {...modalOpts}>

//     </Modal>
//   )
// }

// export default DetailModal;

export default class DetailModal extends React.Component {

  state = {
    list: _.get(this.props, 'item.invoices', []),
    showAdd: false,
  }

  onInvoiceUpdate = (data, index) => {
    const arr = this.state.list || [];

    if (data.amount === "") data.amount = 0;
    if (data.receivedAmount === "") data.receivedAmount = 0;

    if(index == -1){
      arr.push(data);

      this.setState({
        showAdd: false,
      })
    } else {
      arr.splice(index, 1)
      arr.splice(index, 0, data);
    }

    this.setState({
      list: arr,
    })
    message.success("编辑成功，请点击“提交”按钮进行数据同步！");
  }

  handleOkClick = () => {
    const { item, onOk } = this.props;

    onOk ({
      contractId: item.id,
      invoices: this.state.list
    })
    // console.log("detail modal: ", item.id);
  }

  handleAddToggle = () => {
    this.setState({
      showAdd: !this.state.showAdd,
    })
  }

  render() {
    const { item, title, visible, onCancel } = this.props;
    const { id, customerName, signedDate, signedBy, content, shouldReceive } = item;

    const modalOpts = {
      title,
      visible,
      onOk: this.handleOkClick,
      onCancel,
      okText: '提交',
      cancelText: '取消',
      width: '900px',
      maskClosable: false
    }

    const invoiceEditProps = {
      item: {},
      index: -1,
      onInvoiceUpdate: this.onInvoiceUpdate,
      onAddToggle: this.handleAddToggle
    }

    return (
      <Modal { ...modalOpts }>
        <Row>
          <Col span={5}>
            <strong>合同编号: </strong>
            <span>{ id }</span>
          </Col>
          <Col span={10}>
            <strong>客户名称: </strong>
            <span>{ customerName }</span>
          </Col>
          <Col span={5}>
            <strong>签订时间: </strong>
            <span>{ moment(signedDate).format('YYYY-MM-DD') }</span>
          </Col>
          <Col span={4}>
            <strong>签约人: </strong>
            <span>{ signedBy }</span>
          </Col>
        </Row>
        <Row className="marginTopMd">
          <Col span={24}>
            <strong>合同内容: </strong>
            <span>{ content }</span>
          </Col>
        </Row>
        <Row className="marginTopLg">
          <Col style={{fontSize: "20px"}}>分期信息</Col>
          {
            _.map(shouldReceive, (item, index) => {
              return (
                <Row key={index} style={{background: "#eee", padding: "10px", border: "1px solid #eee", margin: "10px auto"}}>
                  <Col span={6}>{item.title}</Col>
                  <Col span={6}>
                    <span>应收金额：</span>
                    <span>{item.defaultAmount}</span>
                  </Col>
                  <Col span={6}>
                    <span>应收日期：</span>
                    <span>{moment(item.shouldReceivedAt).format('YYYY-MM-DD')}</span>
                  </Col>
                </Row>
              )
            })
          }
        </Row>

        <Row className="marginTopLg">
          {this.state.list && <Col style={{fontSize: "20px"}}>回款记录</Col>}
          {
            _.map(this.state.list, (item, index) => {
              const props = {
                item,
                index,
                onInvoiceUpdate: this.onInvoiceUpdate,
                onAddToggle: this.handleAddToggle,
              }
              console.log(props)
              return (
                <Row key={index} className="marginTopMd">
                  <InvoiceItem { ...props } />
                </Row>
              )
            })
          }
        </Row>
        {!this.state.showAdd &&
          <Row className="marginTopLg">
            <Col span={18}></Col>
            <Col span={6}>
              <Button
                type="primary"
                onClick={this.handleAddToggle}
                className="pullRight"
              >新增回款信息</Button>
            </Col>
          </Row>
        }
        {this.state.showAdd &&
          <InvoiceEdit {...invoiceEditProps}/>
        }
      </Modal>
    )
  }
}
