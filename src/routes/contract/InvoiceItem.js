import React from 'react';
import moment from 'moment';

import style from './InvoiceItem.css'
import InvoiceEdit from './InvoiceEdit';

import { Row, Col, Form, Icon } from 'antd';
const FormItem = Form.Item;

class InvoiceItem extends React.Component {

  state = {
    editMode: false
  }

  toggleMode = () => {
    this.setState({
      editMode: !this.state.editMode,
    })
  }

  render() {
    const { item, index, onInvoiceUpdate, onAddToggle } = this.props;
    const editProps = {
      item,
      index,
      onInvoiceUpdate,
      onAddToggle,
      onToggleMode: this.toggleMode
    }
    console.log("from item: ", editProps);

    return (
      <div>
      {!this.state.editMode &&
        <div>
          <Row className={style.title}>
            <Col span={8}>
              <span>发票编号：</span>
              <span>{item.id}</span>
            </Col>
            <Col span={7}>
              <span>开票金额：</span>
              <span>{item.amount}</span>
            </Col>
            <Col span={8}>
              <span>开票日期：</span>
              <span>{moment(item.createdAt).format('YYYY-MM-DD')}</span>
            </Col>
            <Col span={1}><Icon type="edit" onClick={this.toggleMode} /></Col>
          </Row>
          <Row className={style.title}>
            <Col span={12}>
              <span>实收日期：</span>
              <span>{moment(item.receivedAt).format('YYYY-MM-DD')}</span>
            </Col>
            <Col span={12}>
              <span>实收金额：</span>
              <span>{item.receivedAmount}</span>
            </Col>
          </Row>
        </div>
        ||
        <InvoiceEdit {...editProps} />
        }
        </div>
    )
  }
}

export default InvoiceItem
