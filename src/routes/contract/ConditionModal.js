import React from 'react';
import classnames from 'classnames';

import style from './ConditionModal.css';

import { Form, Row, Col, Button, Input, Select, Radio, DatePicker } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const RangePicker = DatePicker.RangePicker;

const ConditionModal = ({
  visible,
  onFetch,
  onClose,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
}) => {

  const cls = classnames({
    [`${style.panel}`]: true,
    [`${style.active}`]: visible
  })

  const handleSubmitClick = () => {
    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
      }
      if (data.shouldReceivedAt) {
        data.shouldReceivedAt = {
          start: data.shouldReceivedAt[0],
          end: data.shouldReceivedAt[1]
        }
      }
      if (data.signedDate) {
        data.signedDate = {
          start: data.signedDate[0],
          end: data.signedDate[1]
        }
      }
      if (data.invoiceDate) {
        data.invoiceDate = {
          start: data.invoiceDate[0],
          end: data.invoiceDate[1]
        }
      }
      onFetch(data);
    })
  }

  return (
    <div className={cls}>
      <Form>
        <Row gutter={15}>
          <Col span={8}>
            <FormItem label="按客户名称">
              {getFieldDecorator('customerName')(
                <Input placeholder="客户名称" />
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label="按客户类型">
              {getFieldDecorator('customerType')(
                <RadioGroup>
                  <RadioButton value={undefined}>全部</RadioButton>
                  <RadioButton value="直营">直营</RadioButton>
                  <RadioButton value="分销">分销</RadioButton>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label="按地区">
              {getFieldDecorator('zone')(
                <Select>
                  <Option value={undefined}>全部</Option>
                  <Option value="华南">华南</Option>
                  <Option value="华中">华中</Option>
                  <Option value="华北">华北</Option>
                  <Option value="华东">华东</Option>
                  <Option value="东北">东北</Option>
                  <Option value="西南">西南</Option>
                  <Option value="西北">西北</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={15}>
          <Col span={6}>
            <FormItem label="按签约人">
              {getFieldDecorator('signedBy')(
                <Input placeholder="签约人姓名" />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="按签约日期">
              {getFieldDecorator('signedDate')(
                <RangePicker />
              )}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem label="按经销商">
              {getFieldDecorator('dealerName')(
                <Input placeholder="经销商名称" />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={15}>
          <Col span={12}>
            <FormItem label="按应收日期">
              {getFieldDecorator('shouldReceivedAt')(
                <RangePicker />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="按回款日期">
              {getFieldDecorator('invoiceDate')(
                <RangePicker />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'center' }}>
            <Button
              type="primary"
              htmlType="submit"
              className="margintRightLg"
              onClick={handleSubmitClick}
            >查询</Button>
            <Button style={{ marginLeft: 8 }} onClick={onClose}>
              取消
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default Form.create()(ConditionModal);