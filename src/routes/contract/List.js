import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'lodash';

import { Table, Modal } from 'antd';
import DropOption from '../../components/dropOption';

const confirm = Modal.confirm


const List = ( { onDeleteItem, onEditItem, onShowDetail, list, pagination, onChange }) => {

  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onShowDetail(record);
    } else if (e.key === '2') {
      onEditItem(record);
    } else if (e.key === '3') {
      confirm({
        title: '确定要删除本条记录？',
        okText: "确认",
        cancelText: "取消",
        onOk () {
          onDeleteItem(record.id)
        },
      })
    }
  }

  const columns = [{
    title: ' 编号',
    dataIndex: 'id',
    key: 'id',
  }, {
    title: '客户名称',
    dataIndex: 'customerName',
    key: 'customerName',
  }, {
    title: '区域',
    dataIndex: 'zone',
    key: 'zone',
  }, {
    title: '总金额',
    dataIndex: 'amount',
    key: 'amount',
  }, {
    title: '签约人',
    dataIndex: 'signedBy',
    key: 'signedBy',
  }, {
    title: '签订日期',
    dataIndex: 'signedDate',
    key: 'signedDate',
    render: (text) => (
      <span>{moment(text).format('YYYY-MM-DD')}</span>
    )
  }, {
    title: '开始日期',
    dataIndex: 'startDate',
    key: 'startDate',
    render: (text) => (
      <span>{moment(text).format('YYYY-MM-DD')}</span>
    )
  }, {
    title: '结束日期',
    dataIndex: 'endDate',
    key: 'endDate',
    render: (text) => (
      <span>{moment(text).format('YYYY-MM-DD')}</span>
    )
  }, {
    title: '应收合计',
    dataIndex: 'shouldReceivedAmount',
    key: 'shouldReceivedAmount',
  }, {
    title: '已収合计',
    dataIndex: 'receivedAmount',
    key: 'receivedAmount',
  }, {
    title: '未收合计',
    dataIndex: 'notReceivedAmount',
    key: 'notReceivedAmount',
  }, {
    title: '开票合计',
    dataIndex: 'invoiceAmount',
    key: 'invoiceAmount',
  }, {
    title: '操作',
    render: (text, record) => (
      //<a onClick={() => handleDetailClick(text, record)}>详情</a>
      <DropOption 
        onMenuClick={e => handleMenuClick(record, e)} 
        menuOptions={[{ key: '1', name: '详情' }, { key: '2', name: '编辑' }, { key: '3', name: '删除' }]} 
      />
    )
  }];

  const array = _.map(list, (item, i) => {
    item.key = i;
    return item;
  })

  return (
    <Table 
      columns={ columns }
      dataSource={ array }
      pagination={ pagination }
      onChange={ onChange }
    />
  )
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  onShowDetail: PropTypes.func,
  list: PropTypes.array,
}

export default List;  