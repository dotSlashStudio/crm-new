import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';

import config from '../../utils/config';

import { Row, Col, Input, Select, Radio, Button, Icon } from 'antd';
const Search = Input.Search;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const Filter = ({
  query,
  onSearch,
  onChangeType,
  onChangeSection,
  onShowMoreCondition,
  onExport,
  onImport,
  onCreate
}) => {

  const handleSectionChange = (value) => {
    if (value === '全部') value = undefined
    onChangeSection(value);
  }

  const handleTypeChange = (e) => {
    let value = e.target.value;
    if (value === '全部') value = undefined
    onChangeType(value)
  }

  const handleSearch = (value) => {
    if (value === '') value = undefined
    onSearch(value)
  }

  const handleShowMoreCondition = () => {
    onShowMoreCondition();
  }

  const handleExportClick = () => {
    onExport()
  }

  const handleImportClick = () => {

  }

  const handleCreateClick = () => {
    onCreate();
  }

  // const qs = queryString.stringify(query);

  const exportUrl = `${config.api}/export-contracts?query=${JSON.stringify(query)}`
  return (
    <Row gutter={15}>
      <Col span={5}>
        <Search
          placeholder="按合同名称搜索"
          onSearch={handleSearch}
          style={{ width: "100%" }}
        />
      </Col>
      <Col span={2}>
        <Select defaultValue="全部" style={{ width: "100%" }} onChange={handleSectionChange}>
          <Option value="全部">全部</Option>
          <Option value="华南">华南</Option>
          <Option value="华中">华中</Option>
          <Option value="华北">华北</Option>
          <Option value="华东">华东</Option>
          <Option value="东北">东北</Option>
          <Option value="西南">西南</Option>
          <Option value="西北">西北</Option>
        </Select>
      </Col>
      <Col span={4}>
        <RadioGroup defaultValue="全部" onChange={handleTypeChange}>
          <RadioButton value="全部">全部</RadioButton>
          <RadioButton value="直营">直营</RadioButton>
          <RadioButton value="分销">分销</RadioButton>
        </RadioGroup>
      </Col>
      <Col span={1}>
        <Button
          type="danger"
          icon="filter"
          onClick={handleShowMoreCondition}
        >更多条件</Button>
      </Col>
      <Col span={2}>
      </Col>
      <Col span={3}>
        {false &&
          <Button
            type="primary"
            icon="export"
            onClick={handleExportClick}

          >导出筛选结果</Button>
        }
        <a href={exportUrl} className="btnPrimary">
          <Icon className="marginRightMd" type="export" />
          <span>导出筛选结果</span>
        </a>
      </Col>
      <Col span={2}>
        <Button
          type="danger"
          icon="select"
          disabled
          onClick={handleImportClick}
        >导入合同</Button>
      </Col>
      <Col span={3}>
      </Col>
      <Col span={2}>
        <Button
          type="primary"
          icon="file-add"
          onClick={handleCreateClick}
        >创建合同</Button>
      </Col>
    </Row>
  )
}

Filter.propTypes = {
  onSearch: PropTypes.func,
  onChangeType: PropTypes.func,
  onChangeSection: PropTypes.func,
  onShowMoreCondition: PropTypes.func,
  onExport: PropTypes.func,
  onImport: PropTypes.func,
  onCreate: PropTypes.func
}

export default Filter;
