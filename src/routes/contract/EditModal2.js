import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import { Modal, Form, Input, Radio, DatePicker, InputNumber, Select } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

class EditModal extends React.Component {

  state = {
    showDealer: _.get(this.props, 'editModalProps.item.customerType') === "分销",
  }

  handleOk = () => {
    const { editModalProps: {item, onOk}, form: { validateFields, getFieldsValue } } = this.props;

    validateFields((errors) => {
      if (errors) {
        return
      }
      const data = {
        ...getFieldsValue(),
        key: item.key,
      }
      onOk(data);
    })
  }

  handleTypeChange = (e) => {
    const value = e.target.value;
    if (value === '分销') {
      this.setState({
        showDealer: true
      })
    } else {
      this.setState({
        showDealer: false
      })
    }
  }

  render() {

    const { editModalProps, form: { getFieldDecorator } } = this.props;
    const { item } = editModalProps;

    console.log(item);
    const formItemLayout = {
      labelCol: {
        span: 6,
      },
      wrapperCol: {
        span: 14,
      },
    }

    const modalOpts = {
      ...editModalProps,
      okText: '确认',
      cancelText: '取消',
      onOk: this.handleOk,
      maskClosable: false
    }

    return (
      <Modal {...modalOpts}>
        <Form layout="horizontal">
          <FormItem label="合同编号" hasFeedback {...formItemLayout}>
            {getFieldDecorator('id', {
              initialValue: item.id,
              rules: [
                {
                  required: true,
                  message: '请填写合同编号'
                },
              ],
            })(<Input disabled={item.id != null} />)}
          </FormItem>
          <FormItem label="客户名称" hasFeedback {...formItemLayout}>
            {getFieldDecorator('customerName', {
              initialValue: item.customerName,
              rules: [
                {
                  required: true,
                },
              ],
            })(<Input />)}
          </FormItem>
          <FormItem label="客户类型" hasFeedback {...formItemLayout}>
            {getFieldDecorator('customerType', {
              initialValue: item.customerType,
              rules: [
                {
                  required: true,
                },
              ],
            })(<Radio.Group onChange={this.handleTypeChange}>
              <Radio value="直营">直营</Radio>
              <Radio value="分销">分销</Radio>
               </Radio.Group>)}
          </FormItem>
          {this.state.showDealer &&
          <FormItem label="经销商" hasFeedback {...formItemLayout}>
            {getFieldDecorator('dealerName', {
              initialValue: item.dealerName,
              rules: [
                {
                  required: true,
                },
              ],
            })(<Input />)}
          </FormItem>
          }
          <FormItem label="区域" hasFeedback {...formItemLayout}>
            {getFieldDecorator('zone', {
              initialValue: item.zone,
              rules: [
                {
                  required: true,
                },
              ],
            })(
              <Select>
                <Option value="华南">华南</Option>
                <Option value="华中">华中</Option>
                <Option value="华北">华北</Option>
                <Option value="华东">华东</Option>
                <Option value="东北">东北</Option>
                <Option value="西南">西南</Option>
                <Option value="西北">西北</Option>
              </Select>
            )}
          </FormItem>
          <FormItem label="合同金额" hasFeedback {...formItemLayout}>
            {getFieldDecorator('amount', {
              initialValue: item.amount,
              rules: [
                {
                  required: true,
                },
              ],
            })(<Input />)}
          </FormItem>
          <FormItem label="签约人" hasFeedback {...formItemLayout}>
            {getFieldDecorator('signedBy', {
              initialValue: item.signedBy,
              rules: [
                {
                  required: true,
                },
              ],
            })(<Input />)}
          </FormItem>
          <FormItem label="签约日期" hasFeedback {...formItemLayout}>
            {getFieldDecorator('signedDate', {
              initialValue: item.signedDate ? moment(item.signedDate) : null,
              rules: [
                {
                  required: true,
                },
              ],
            })(<DatePicker />)}
          </FormItem>
          <FormItem label="服务开始日期" hasFeedback {...formItemLayout}>
            {getFieldDecorator('startDate', {
              initialValue: item.startDate ? moment(item.startDate) : null,
              rules: [
                {
                  required: true,
                },
              ],
            })(<DatePicker />)}
          </FormItem>
          <FormItem label="服务结束日期" hasFeedback {...formItemLayout}>
            {getFieldDecorator('endDate', {
              initialValue: item.endDate ? moment(item.endDate) : null,
              rules: [
                {
                  required: true,
                },
              ],
            })(<DatePicker />)}
          </FormItem>
          <FormItem label="合同期数" hasFeedback {...formItemLayout}>
            {getFieldDecorator('period', {
              initialValue: _.get(item, 'invoices.length') || 3,
              rules: [
                {
                  required: true,
                  type: 'number',
                },
              ],
            })(<InputNumber min={1} max={10} />)}
          </FormItem>
          <FormItem label="合同内容" hasFeedback {...formItemLayout}>
            {getFieldDecorator('content', {
              initialValue: item.content,
            })(<TextArea rows={3} />)}
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(EditModal);