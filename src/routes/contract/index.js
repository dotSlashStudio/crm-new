import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import queryString from 'query-string';
import Filter from './Filter';
import List from './List';
import EditModal from './EditModal2';
import DetailModal from './DetailModal';
import ConditionModal from './ConditionModal';

import { Row, Card } from 'antd';


const ContractPage = ({
  location, 
  dispatch, 
  list, 
  after,
  hasNext,
  pagination,
  editModalVisible,
  editModalType,
  currentItem,
  detailModalVisible,
  conditionModalVisible
}) => {

  const { pathname, query } = location;
  const filterProps = {
    query,
    onSearch (value) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          customerName: value,
        }
      }))
    },
    onChangeType (value) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          customerType: value,
        }
      }))
    },
    onChangeSection (value) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          zone: value
        }
      }))
    },
    onShowMoreCondition () {
      dispatch({
        type: 'contract/showConditionModal',
      })
    },
    onExport () {
      dispatch({
        type: 'contract/exportFile',
      })
    },
    onImport () {

    },
    onCreate () {
      dispatch({
        type: 'contract/showEditModal',
        payload: {
          editModalType: 'create',
        },
      })
    }
  }
  
  const editModalProps = {
    visible: editModalVisible,
    title: editModalType === 'create' ? '创建合同' : '编辑合同',
    item: editModalType === 'create' ? {} : currentItem,
    onOk (values) {
      dispatch({
        type:  `contract/${editModalType}`,
        payload: values
      })
    },
    onCancel () {
      dispatch({
        type: 'contract/hideEditModal',
      })
    }
  }

  const detailModalProps = {
    visible: detailModalVisible,
    title: '回款详情',
    item: currentItem,
    onOk ({ contractId, invoices }) {
      dispatch({
        type:  'contract/updateInvoice',
        payload: { contractId, invoices }
      })
    },
    onCancel () {
      dispatch({
        type: 'contract/hideDetailModal',
      })
    }
  }

  const conditionModalProps = {
    visible: conditionModalVisible,
    onFetch(values) {
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...values
        }
      }))
      dispatch({
        type: 'contract/hideConditionModal'
      })
    },
    onClose () {
      dispatch({
        type: 'contract/hideConditionModal'
      })
    }
  }

  const listProps = {
    list,
    pagination,
    onChange (page) {
      console.log("index: ", page)
      dispatch(routerRedux.push({
        pathname,
        query,
        search: queryString.stringify({
          page: page.current,
        }),
      }))
    },
    onDeleteItem (id) {
      dispatch({
        type: 'contract/remove',
        payload: { contractId: id }
      })
    },
    onEditItem (item) {
      dispatch({
        type: 'contract/showEditModal',
        payload: {
          editModalType: 'update',
          currentItem: item,
        },
      })
    },
    onShowDetail (item) {
      dispatch({
        type: 'contract/showDetailModal',
        payload: {
          currentItem: item,
        },
      })
    }
  }

  return (
    <Card style={{minHeight: "600px"}}>
      <Filter {...filterProps} />
      <Row className="marginTopLg">
        <List {...listProps} />
      </Row>
      {editModalVisible && <EditModal editModalProps = { editModalProps } />}
      {detailModalVisible && <DetailModal { ...detailModalProps } />}
      <ConditionModal { ...conditionModalProps } />
    </Card>
  )
}


function mapStateToProps(state) {
  //const { list, total, page, editModalVisible, editModalType, editModalType } = state.contract;
  console.log(state);
  return {
    loading: state.loading.models.contract,
    ...state.contract
  };
  
}

export default connect(mapStateToProps)(ContractPage);