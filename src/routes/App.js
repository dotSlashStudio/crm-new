import React from 'react'
import { connect } from 'dva'
import { withRouter } from 'dva/router'

import { Row, Col, Avatar } from 'antd';

import styles from './app.css';

import Sidebar from '../components/sidebar/Sidebar';


const App = ({
  children, dispatch, app, loading, location,
}) => {

  const { pathname } = location;
  let title;
  switch (pathname) {
    case "/dashboard":
      title = "总览";
      break;
    case "/contract":
      title = "合同管理";
      break;
    case "/customer":
      title = "客户管理";
      break;
    case "/employment":
      title = "员工管理";
      break;
    case "/setting":
      title = "设置";
      break;
    default:
      title = "总览";
  }

  return (
    <div>
      <div className={styles.leftBlock}>
        <Sidebar location={location} />
      </div>
      <div className={styles.rightBlock}>
        <div className={styles.header}>
          <Row>
            <Col span={22} className={styles.title}>{title}</Col>
            <Col span={1}>
              <Avatar icon="user" className={styles.avatar}/>
            </Col>
            <Col span={1}>
              <span>李奉天</span>
            </Col>
          </Row>
        </div>
        <div className={styles.content}>
          { children }
        </div>
      </div>
    </div>
  )
}

export default withRouter(connect(({ app, loading }) => ({ app, loading }))(App))
