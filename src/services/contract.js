import config from '../utils/config';
import request from '../utils/request';
import _ from 'lodash';
import moment from 'moment';
import { message } from 'antd';

const url = `${config.api}/contract`;
const exportUrl = `${config.api}/export-contracts`
// const importUrl = `${config.api}/export-contracts`

const fragment = `
  count
  after
  hasNext
  data {
    id
    customerName
    customerType
    dealerName
    zone
    amount
    signedBy
    signedDate
    shouldReceivedAmount
    receivedAmount
    invoiceAmount
    notReceivedAmount
    startDate
    endDate
    content
    shouldReceive {
      title
      defaultAmount
      shouldReceivedAt
    }
    invoices {
      createdAt
      id
      amount
      receivedAt
      receivedAmount
    }
  }
`

const fetch = (variables) => {
  const query = `
    query (
      $query: ContractQueryInput
      $skip: Int
      $sort: Sort
      $order: Order
      $after: String
    ) {
      contracts (
        query: $query
        limit: 10
        skip: $skip
        sort: $sort
        order: $order
        after: $after
      ) {
        ${fragment}
      }
    }
  `
  const { page=1 } = variables;
  const skip = (page - 1) * 10;
  return request(url, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query, variables: { ...variables, skip } })
  })
  .then((res) => {
    return _.get(res, 'data.data.contracts');
  })
}

const generateShouldReceive = (amount, startDate, endDate, period) => {
  let invoice = [];
  let diff = moment(endDate).diff(moment(startDate), 'day');
  diff = Math.ceil(diff / period);
  for(let i = 0; i < period; i++) {
    const obj = {
      title: `第${i+1}期`,
      defaultAmount: Number(amount/period),
      shouldReceivedAt: moment(startDate).add(diff*i, 'day').format('YYYY-MM-DD') ,
    };
    invoice.push(obj);
  }
  return invoice;
}

const create = (values) => {
  const { amount, startDate, endDate, period } = values;
  const shouldReceive = generateShouldReceive(amount, startDate, endDate, period);
  const query = `
    mutation(
      $id: String!
      $customerName: String!
      $customerType: String!
      $dealerName: String
      $zone: String!
      $amount: Float!
      $content: String
      $signedBy: String!
      $signedDate: String!
      $startDate: String!
      $endDate: String!
      $shouldReceive: [ShouldReceiveInput]
    ) {
      createContract (
        data: {
          id: $id
          customerName: $customerName
          customerType: $customerType
          dealerName: $dealerName
          zone: $zone
          amount: $amount
          content: $content
          signedBy: $signedBy
          signedDate: $signedDate
          startDate: $startDate
          endDate: $endDate
          shouldReceive: $shouldReceive
        }
      )
    }
  `
  return request(url, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query, variables: {...values, shouldReceive} })
  })
  .then((res) => {
    return _.get(res, 'data.data.createContract');
  })
}


const remove = ({ contractId }) => {

  const query = `
    mutation(
      $contractId: String!
    ) {
      deleteContract (
        contractId: $contractId
      )
    }
  `
  return request(url, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query, variables: { contractId } })
  })
  .then((res) => {
    return _.get(res, 'data.data.deleteContract');
  })
}


const update = (values) => {

  const contractId = values.contractId;

  const query = `
    mutation(
      $contractId: String!
      $id: String!
      $customerName: String!
      $customerType: String!
      $dealerName: String
      $zone: String!
      $amount: Float!
      $content: String
      $signedBy: String!
      $signedDate: String!
      $startDate: String!
      $endDate: String!
    ) {
      updateContract (
        contractId: $contractId
        data: {
          id: $id
          customerName: $customerName
          customerType: $customerType
          dealerName: $dealerName
          zone: $zone
          amount: $amount
          content: $content
          signedBy: $signedBy
          signedDate: $signedDate
          startDate: $startDate
          endDate: $endDate
        }
      )
    }
  `
  return request(url, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query, variables: {contractId, ...values} })
  })
  .then((res) => {
    return _.get(res, 'data.data.updateContract');
  })

}

const updateInvoice = ({ contractId, invoices }) => {
  const query = `
    mutation(
      $contractId: String!
      $invoices: [InvoiceInput!]!
    ) {
      updateInvoices (
        contractId: $contractId
        invoices: $invoices
      )
    }
  `
  // console.log('service: ', contractId)
  return request(url, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query, variables: {contractId, invoices} })
  })
  .then((res) => {
    return _.get(res, 'data.data.updateInvoices');
  })
}

const exportFile = (query) => {
  const q = query ? query : {};
  // request(exportUrl, {
  //   method: "GET",
  //   headers: { 'Content-Type': 'application/file' },
  //   query: JSON.stringify({ query: q })
  // })
  request(`${exportUrl}?query=${JSON.stringify({ query: q })}`)
}

const importFile = ({ query }) => {

}

export { fetch, create, remove, update, updateInvoice, exportFile, importFile };

